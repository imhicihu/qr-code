![stability-wip](https://bitbucket.org/repo/ekyaeEE/images/3278295154-status_archived.png)
![Bitbucket issues](https://bitbucket.org/repo/ekyaeEE/images/1555006384-issues_closed.png)
![internaluse-green](https://bitbucket.org/repo/ekyaeEE/images/3847436881-internal_use_stable.png)

# RATIONALE #

* Checklist to generate qr codes for any digital asset
* This repo is a living document that will grow and adapt over time
![qrcoding.jpeg](https://bitbucket.org/repo/BgLpKje/images/2070783795-qrcoding.jpeg)

### What is this repository for? ###

* Quick summary
    - Procedures to generate qr codes for academic papers in symposia, academic events, _etc_.
* Version 1.01

### How do I get set up? ###

* Configuration
    - Check [`procedure.md`](https://bitbucket.org/imhicihu/qr-code/src/01bedc6e0d0c1eac27bf360756d550e270694289/Procedure.md?at=master)
* Dependencies
    - Check [`colophon.md`](https://bitbucket.org/imhicihu/qr-code/src/5a5f293c3c923b5b55f86662f1831e81057a8b0d/Colophon.md?at=master&fileviewer=file-view-default=)
* Database configuration
    - Out of the scope of this repo
* How to run tests
    - The same QR code generator encode/decode the image generated.
    ![decoder.jpeg](https://bitbucket.org/repo/BgLpKje/images/4039695448-decoder.jpeg)

### Related repositories ###

* Some repositories linked with this project:
     - [Conferences](https://bitbucket.org/imhicihu/conferences/src/master/)
     - [Streaming](https://bitbucket.org/imhicihu/streaming/src/master/)

### Source ###

* Check them on [here](https://bitbucket.org/imhicihu/qr-code/src)

### Issues ###

* Check them on [here](https://bitbucket.org/imhicihu/qr-code/issues)

### Changelog ###

* Please check the [Commits](https://bitbucket.org/imhicihu/qr-code/commits/) section for the current status


### Who do I talk to? ###

* Repo owner or admin
    - Contact `imhicihu` at `gmail` dot `com`
* Other community or team contact
    - Contact is _enable_ on the [board](https://bitbucket.org/imhicihu/qr-code/addon/trello/trello-board) of this repo. (You need a [Trello](https://trello.com/) account)

### Code of Conduct

* Please, check our [Code of Conduct](https://bitbucket.org/imhicihu/qr-code/src/master/code_of_conduct.md)

### Legal ###

* All trademarks are the property of their respective owners.

### License ###

* The content of this project itself is licensed under the ![MIT Licence](https://bitbucket.org/repo/ekyaeEE/images/2049852260-MIT-license-green.png)